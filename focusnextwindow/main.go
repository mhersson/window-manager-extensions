package main

import (
	"log"
	"math"
	"os"

	"github.com/BurntSushi/xgb/xproto"
	"github.com/BurntSushi/xgbutil"
	"github.com/BurntSushi/xgbutil/ewmh"
	"github.com/BurntSushi/xgbutil/xprop"
	"github.com/BurntSushi/xgbutil/xwindow"
)

func getClosestWindow(xu *xgbutil.XUtil, dir string, windows []*xwindow.Window) xproto.Window {
	var lw []*xwindow.Window
	// Get the active window and set its geometry
	a, err := ewmh.ActiveWindowGet(xu)
	if err != nil {
		log.Fatal(err)
	}

	awin := xwindow.New(xu, a)

	_, _ = awin.Geometry()

	for _, v := range windows {
		switch dir {
		case "left":
			if v.Geom.X() < awin.Geom.X() {
				lw = append(lw, v)
			}
		case "right":
			if v.Geom.X() > awin.Geom.X()+awin.Geom.Width() {
				lw = append(lw, v)
			}
		case "up":
			if v.Geom.Y() < awin.Geom.Y() {
				lw = append(lw, v)
			}
		case "down":
			if v.Geom.Y() > awin.Geom.Y()+awin.Geom.Height() {
				lw = append(lw, v)
			}
		}
	}

	switch len(lw) {
	case 0:
		// Return current window if there are no
		// windows in the given direction
		return awin.Id
	case 1:
		return lw[0].Id
	default:
	}

	var closestWindow xproto.Window

	var distance float64

	d := math.MaxFloat64

	for _, w := range lw {
		distance = math.Sqrt(
			math.Pow(float64(awin.Geom.X()-w.Geom.X()), 2) +
				math.Pow(float64(awin.Geom.Y()-w.Geom.Y()), 2))
		if distance < d {
			d = distance
			closestWindow = w.Id
		}
	}

	return closestWindow
}

func main() {
	if len(os.Args) != 2 {
		log.Fatalf("Program takes exactly one argument (direction), %d given", len(os.Args)-1)
	}

	xu, err := xgbutil.NewConn()
	if err != nil {
		log.Fatal(err)
	}

	windows, err := ewmh.ClientListGet(xu)
	if err != nil {
		log.Fatal(err)
	}

	// Get all visble windows in the current workspace
	// (WM_STATE[0] == 1_seems to match this criteria
	viswin := []*xwindow.Window{}

	for _, w := range windows {
		xwin := xwindow.New(xu, w)
		_, _ = xwin.Geometry()

		d, _ := xprop.GetProperty(xu, w, "WM_STATE")
		if int(d.Value[0]) == 1 {
			viswin = append(viswin, xwin)
		}
	}

	target := getClosestWindow(xu, os.Args[1], viswin)

	xproto.WarpPointer(xu.Conn(), 0, target, 0, 0, 0, 0, 6, 12)

	// _ = ewmh.ActiveWindowReq(xu, target)
	_ = ewmh.ActiveWindowSet(xu, target)
}
