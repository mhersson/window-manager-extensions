package main

import (
	"log"
	"strconv"

	"github.com/BurntSushi/xgbutil"
	"github.com/BurntSushi/xgbutil/ewmh"
	"github.com/BurntSushi/xgbutil/xinerama"
	"github.com/BurntSushi/xgbutil/xrect"
	"github.com/BurntSushi/xgbutil/xwindow"
)

// Should probably have this in a config file... whatever
var prefs = []MasterArea{
	{"2560x1440", 735, 15, 1170, 1400},
	{"1920x1080", 690, 15, 1215, 1050},
}

//MasterArea type describes position and size of a master area for a resolution
type MasterArea struct {
	res        string
	x, y, w, h int
}

// Return the number of the screen where the window is currently positioned
func getCurrentScreen(win *xwindow.Window, screens xinerama.Heads) int {
	// Get center coordinates of the active window
	x := win.Geom.X() + (win.Geom.Width() / 2)
	y := win.Geom.Y() + (win.Geom.Height() / 2)

	for i, s := range screens {
		if s.X() < x && x < s.X()+s.Width() &&
			s.Y() < y && y < s.Y()+s.Height() {
			return i
		}
	}
	return -1
}

func moveAndResize(xu *xgbutil.XUtil, win *xwindow.Window, s xrect.Rect, m MasterArea) {

	win.MoveResize(m.x+s.X(), m.y+s.Y(), m.w, m.h)
	win.WMMoveResize(m.x+s.X(), m.y+s.Y(), m.w, m.h)

	ewmh.ActiveWindowReq(xu, win.Id)
}

func main() {
	xu, err := xgbutil.NewConn()
	if err != nil {
		log.Fatal(err)
	}

	// Get the active window and load its geometry
	a, err := ewmh.ActiveWindowGet(xu)
	if err != nil {
		log.Fatal(err)
	}
	win := xwindow.New(xu, a)
	win.Geometry()

	screens, err := xinerama.PhysicalHeads(xu)
	if err != nil {
		log.Fatal(err)
	}

	// Get screen of active window
	cs := getCurrentScreen(win, screens)

	res := strconv.Itoa(screens[cs].Width()) + "x" + strconv.Itoa(screens[cs].Height())

	var master MasterArea
	for _, m := range prefs {
		if m.res == res {
			master = m
			break
		}
	}

	moveAndResize(xu, win, screens[cs], master)

}
