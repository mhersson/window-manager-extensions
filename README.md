## README

Window Manager Extensions is a set of small programs to add window focus and movement functionality.
Written for, and tested on cwm (except for toggle-screns which is only tested on i3),
but should work on any EWMH compliant window manager.

### focusnextwindow
focusnextwindow moves focus to the closest window in a given direction <left|down|up|right>.
Program takes one argument, direction.

### swapwindow
swapwindow swaps position and size with the closest window in a given direction  <left|down|up|right>.
If no direction is given the active window is swapped with the largest winodw on the current monitor.
Much like the "move to master area" functionality of many tiling WMs. Program takes one argument, direction.

### setmaster
setmaster resizes and positions the active window to a predefined area of the active monitor.
The poperties are set in the source code
>NOTE: cwm does not have a master area. It's just my definition of my primary working area,
coming from tiling WMs calling it master area seems natural though.

### movetoscreen
movetoscreen moves the active window or just the mouse pointer to the next
physical screen (monitor) in a given direction <left|down|up|right>.
Program takes two arguments, direction and "win". If second argument "win" is not set,
movetoscreen only moves the mouse pointer.

### toggle-screens
toggle-screens are for multi gpu setups used with one xsserver instance and two screens.
Screens are not monitors, one xserver screen can contain multiple monitors. Focus is move
between the active window of both screens


### Examples from my .cwmrc
```
# Focus next window
bind-key 4-h "focusnextwindow left"
bind-key 4-j "focusnextwindow down"
bind-key 4-k "focusnextwindow up"
bind-key 4-l "focusnextwindow right"

# Swap position and size
bind-key 4S-h "swapwindow left"
bind-key 4S-j "swapwindow down"
bind-key 4S-k "swapwindow up"
bind-key 4S-l "swapwindow right"
# Swap with master
bind-key 4-Return "swapwindow"

# Resize and position as master
bind-key 4-space "/home/user/.cwm/setmaster"

# Move to screen (move focus)
bind-key CM-Left "movetoscreen left"
bind-key CM-Down "movetoscreen down"
bind-key CM-Up "movetoscreen up"
bind-key CM-Right "movetoscreen right"

# Move to screen (move active window)
bind-key CMS-Left "movetoscreen left win"
bind-key CMS-Down "movetoscreen down win"
bind-key CMS-Up "movetoscreen up win"
bind-key CMS-Right "movetoscreen right win"
```
