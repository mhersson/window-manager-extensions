package main

import (
	"fmt"

	"github.com/BurntSushi/xgb"
	"github.com/BurntSushi/xgbutil"
	"github.com/BurntSushi/xgbutil/ewmh"

	"github.com/BurntSushi/xgb/xproto"
)

func moveFocusToWindow(x *xgb.Conn, src, dst xproto.Window) {
	fmt.Printf("Moving to window: %d\n", int(dst))
	xproto.WarpPointer(x, src, dst, 0, 0, 0, 0, 20, 20)
}

func createWindow(xu *xgbutil.XUtil) {
	setup := xproto.Setup(xu.Conn())
	screen := setup.DefaultScreen(xu.Conn())
	wid, _ := xproto.NewWindowId(xu.Conn())

	xproto.CreateWindow(xu.Conn(), screen.RootDepth, wid, screen.Root,
		0, 0, 500, 500, 0,
		xproto.WindowClassInputOutput, screen.RootVisual, 0, []uint32{})

	xproto.ChangeWindowAttributes(xu.Conn(), wid,
		xproto.CwBackPixel|xproto.CwEventMask,
		[]uint32{ // values must be in the order defined by the protocol
			0xffffffff,
			xproto.EventMaskStructureNotify})

	err := xproto.MapWindowChecked(xu.Conn(), wid).Check()
	if err != nil {
		fmt.Printf("Checked Error for mapping window %d: %s\n", wid, err)
	} else {
		fmt.Printf("Map window %d successful!\n", wid)
	}

	for {
		// This will exit and close the window
		// as soon as it gains focus, or an error occurs
		ev, xerr := xu.Conn().WaitForEvent()
		if ev != nil || xerr != nil {
			return
		}
	}
}

func main() {
	currentXU, err := xgbutil.NewConn()
	if err != nil {
		panic(err)
	}

	display := ":0.0"
	if currentXU.Conn().DefaultScreen == 0 {
		display = ":0.1"
	}

	xu, err := xgbutil.NewConnDisplay(display)
	if err != nil {
		fmt.Println(err)
	}

	clients, _ := ewmh.ClientListGet(xu)

	var win xproto.Window

	if len(clients) >= 1 {
		win, err = ewmh.ActiveWindowGet(xu)
		if err != nil {
			return
		}

		createWindow(xu)
		moveFocusToWindow(xu.Conn(), 0, win)
	} else {
		for i := 0; i <= 2; i++ {
			createWindow(xu)
		}
	}
}
