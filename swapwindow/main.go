package main

import (
	"log"
	"math"
	"os"

	"github.com/BurntSushi/xgb/xproto"
	"github.com/BurntSushi/xgbutil"
	"github.com/BurntSushi/xgbutil/ewmh"
	"github.com/BurntSushi/xgbutil/xevent"
	"github.com/BurntSushi/xgbutil/xinerama"
	"github.com/BurntSushi/xgbutil/xprop"
	"github.com/BurntSushi/xgbutil/xwindow"
)

// Return the number of the screen where the window is currently positioned.
func getCurrentScreen(win *xwindow.Window, screens xinerama.Heads) int {
	// This maybe inaccurate using only the x, y coordinates of the
	// active window if the window is placed between to screens.
	// I'll let it slide for now to see how it works
	for i, s := range screens {
		if s.X() < win.Geom.X() && win.Geom.X() < s.X()+s.Width() &&
			s.Y() < win.Geom.Y() && win.Geom.Y() < s.Y()+s.Height() {
			return i
		}
	}

	return -1
}

func getWindowsOnCurrentScreen(xu *xgbutil.XUtil, win *xwindow.Window, windows []*xwindow.Window) []*xwindow.Window {
	// Get all monitors
	screens, err := xinerama.PhysicalHeads(xu)
	if err != nil {
		log.Fatal(err)
	}

	var onCurrentScreen []*xwindow.Window
	// Get screen of active window
	cs := getCurrentScreen(win, screens)

	// Find all clients on the same screen as the active
	for _, w := range windows {
		if getCurrentScreen(w, screens) == cs {
			onCurrentScreen = append(onCurrentScreen, w)
		}
	}

	return onCurrentScreen
}

// Find the largest client.
func findLargestClient(clients []*xwindow.Window) *xwindow.Window {
	if len(clients) == 0 {
		return nil
	}

	var (
		largest int
		areal   int
	)

	for i, c := range clients {
		a := c.Geom.Width() * c.Geom.Height()
		if a > areal {
			areal = a
			largest = i
		}
	}

	return clients[largest]
}

func swapPositionWithClient(xu *xgbutil.XUtil, awin *xwindow.Window, lwin *xwindow.Window) {
	// save old pos and size
	x, y, w, h := awin.Geom.Pieces()

	awin.MoveResize(lwin.Geom.Pieces())
	_ = awin.WMMoveResize(lwin.Geom.Pieces())
	lwin.MoveResize(x, y, w, h)
	_ = lwin.WMMoveResize(x, y, w, h)

	ad, err := ewmh.WmDesktopGet(xu, awin.Id)
	if err != nil {
		log.Fatal(err)
	}

	ld, err := ewmh.WmDesktopGet(xu, lwin.Id)
	if err != nil {
		log.Fatal(err)
	}

	sendDesktopReq(xu, awin.Id, int(ld))
	sendDesktopReq(xu, lwin.Id, int(ad))

	_ = ewmh.ActiveWindowReq(xu, awin.Id)
}

func sendDesktopReq(xu *xgbutil.XUtil, win xproto.Window, desk int) {
	mstype, err := xprop.Atm(xu, "_NET_WM_DESKTOP")
	if err != nil {
		log.Fatal(err)
	}

	evMask := (xproto.EventMaskSubstructureNotify |
		xproto.EventMaskSubstructureRedirect)

	cm, err := xevent.NewClientMessage(32, win, mstype, desk)
	if err != nil {
		log.Fatal(err)
	}

	_ = xevent.SendRootEvent(xu, cm, uint32(evMask))
}

func getClosestWindow(dir string, awin *xwindow.Window, windows []*xwindow.Window) *xwindow.Window {
	var lw []*xwindow.Window

	for _, v := range windows {
		switch dir {
		case "left":
			if v.Geom.X() < awin.Geom.X() {
				lw = append(lw, v)
			}
		case "right":
			if v.Geom.X() > awin.Geom.X()+awin.Geom.Width() {
				lw = append(lw, v)
			}
		case "up":
			if v.Geom.Y() < awin.Geom.Y() {
				lw = append(lw, v)
			}
		case "down":
			if v.Geom.Y() > awin.Geom.Y()+awin.Geom.Height() {
				lw = append(lw, v)
			}
		}
	}

	if len(lw) == 1 {
		return lw[0]
	}

	var (
		closestWindow *xwindow.Window
		distance      float64
	)

	d := math.MaxFloat64

	for _, w := range lw {
		distance = math.Sqrt(
			math.Pow(float64(awin.Geom.X()-w.Geom.X()), 2) +
				math.Pow(float64(awin.Geom.Y()-w.Geom.Y()), 2))
		if distance < d {
			d = distance
			closestWindow = w
		}
	}

	return closestWindow
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}

	return false
}

func main() {
	xu, err := xgbutil.NewConn()
	if err != nil {
		log.Fatal(err)
	}

	clients, err := ewmh.ClientListGet(xu)
	if err != nil {
		log.Fatal(err)
	}

	// Get all visble windows in the current workspace
	// (WM_STATE[0] == 1_seems to match this criteria
	viswin := []*xwindow.Window{}

	for _, c := range clients {
		xwin := xwindow.New(xu, c)
		_, _ = xwin.Geometry()

		d, _ := xprop.GetProperty(xu, c, "WM_STATE")
		if int(d.Value[0]) == 1 {
			viswin = append(viswin, xwin)
		}
	}

	// Get the active window and load its geometry
	a, err := ewmh.ActiveWindowGet(xu)
	if err != nil {
		log.Fatal(err)
	}

	awin := xwindow.New(xu, a)
	_, _ = awin.Geometry()

	var win *xwindow.Window

	directions := []string{"left", "down", "up", "right"}

	if len(os.Args) == 2 && stringInSlice(os.Args[1], directions) {
		win = getClosestWindow(os.Args[1], awin, viswin)
	} else {
		onscreen := getWindowsOnCurrentScreen(xu, awin, viswin)
		win = findLargestClient(onscreen)
	}

	swapPositionWithClient(xu, awin, win)
}
