package main

import (
	"log"
	"math"
	"os"

	"github.com/BurntSushi/xgbutil/ewmh"
	"github.com/BurntSushi/xgbutil/xwindow"

	"github.com/BurntSushi/xgb"
	"github.com/BurntSushi/xgb/xproto"
	"github.com/BurntSushi/xgbutil"
	"github.com/BurntSushi/xgbutil/xinerama"
	"github.com/BurntSushi/xgbutil/xrect"
)

func getCurrentMonitor(x, y int, monitors xinerama.Heads) int {
	for i, s := range monitors {
		if s.X() < x && x < s.X()+s.Width() &&
			s.Y() < y && y < s.Y()+s.Height() {
			return i
		}
	}

	return 0
}

func getMonitorsInDirection(dir string, mon int, monitors xinerama.Heads) xinerama.Heads {
	currMon := monitors[mon]

	var adjacentMontors xinerama.Heads

	switch dir {
	case "left":
		for _, m := range monitors {
			if m.X() < currMon.X() {
				adjacentMontors = append(adjacentMontors, m)
			}
		}
	case "down":
		for _, m := range monitors {
			if m.Y() > currMon.Y() {
				adjacentMontors = append(adjacentMontors, m)
			}
		}
	case "up":
		for _, m := range monitors {
			if m.Y() < currMon.Y() {
				adjacentMontors = append(adjacentMontors, m)
			}
		}
	case "right":
		for _, m := range monitors {
			if m.X() > currMon.X() {
				adjacentMontors = append(adjacentMontors, m)
			}
		}
	}

	return adjacentMontors
}

func findMonitorXYCenter(mon xrect.Rect) (int16, int16) {
	return int16((mon.Width() / 2) + mon.X()), int16((mon.Height() / 2) + mon.Y())
}

func moveToMonitor(m xrect.Rect, win *xwindow.Window) {
	win.Move(((m.Width()/2)+m.X())-(win.Geom.Width()/2),
		((m.Height()/2)+m.Y())-(win.Geom.Height()/2))

	_ = win.WMMove(((m.Width()/2)+m.X())-(win.Geom.Width()/2),
		((m.Height()/2)+m.Y())-(win.Geom.Height()/2))
}

func findClosestMonitor(monitors xinerama.Heads, x, y int) xrect.Rect {
	var (
		monitor  xrect.Rect
		distance float64
	)

	d := math.MaxFloat64

	for _, m := range monitors {
		distance = math.Sqrt(
			math.Pow(float64(x-m.X()), 2) +
				math.Pow(float64(y-m.Y()), 2))

		if distance < d {
			d = distance
			monitor = m
		}
	}

	return monitor
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}

	return false
}

func main() {
	var directions = []string{"left", "down", "up", "right"}

	X, err := xgb.NewConn()
	if err != nil {
		log.Fatal(err)

		return
	}

	xu, err := xgbutil.NewConn()
	if err != nil {
		log.Fatal(err)

		return
	}

	monitors, err := xinerama.PhysicalHeads(xu)
	if err != nil {
		log.Fatal(err)
	}

	if len(os.Args) == 2 && stringInSlice(os.Args[1], directions) {
		mpos, err := xproto.QueryPointer(X, xu.RootWin()).Reply()
		if err != nil {
			log.Fatal(err)
		}

		currMon := getCurrentMonitor(int(mpos.RootX), int(mpos.RootY), monitors)

		adjMons := getMonitorsInDirection(os.Args[1], currMon, monitors)

		if len(adjMons) == 1 {
			x, y := findMonitorXYCenter(adjMons[0])
			xproto.WarpPointer(X, 0, xu.RootWin(), 0, 0, 0, 0, x, y)
		} else if len(adjMons) > 1 {
			cm := findClosestMonitor(adjMons, monitors[currMon].X(), monitors[currMon].Y())
			x, y := findMonitorXYCenter(cm)
			xproto.WarpPointer(X, 0, xu.RootWin(), 0, 0, 0, 0, x, y)
		}
	} else if len(os.Args) == 3 && stringInSlice(os.Args[1], directions) && os.Args[2] == "win" {
		a, err := ewmh.ActiveWindowGet(xu)
		if err != nil {
			log.Fatal(err)
		}
		awin := xwindow.New(xu, a)
		_, _ = awin.Geometry()

		currMon := getCurrentMonitor(awin.Geom.X(), awin.Geom.Y(), monitors)

		adjMons := getMonitorsInDirection(os.Args[1], currMon, monitors)

		if len(adjMons) == 1 {
			moveToMonitor(adjMons[0], awin)
			_ = ewmh.ActiveWindowReq(xu, awin.Id)
		} else if len(adjMons) > 1 {
			m := findClosestMonitor(adjMons, awin.Geom.X(), awin.Geom.Y())
			moveToMonitor(m, awin)
			_ = ewmh.ActiveWindowReq(xu, awin.Id)
		}
	}
}
